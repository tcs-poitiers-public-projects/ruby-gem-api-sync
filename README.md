# ApiSync

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'api-sync', :git => 'https://gitlab.com/tcs-poitiers-public-projects/ruby-gem-api-sync.git', :tag => 'v0.0.1'
```

The tag is the number of the version required.

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install api-sync

## Usage
### Requiring the module

The ApiSync module needs to be included with `require 'api_sync'` before usage.

### Settings

Before usage of ApiSync methods, you need to initialize api parameters.

```ruby
ApiSync.init(
  :api_url => "https://my_domain.com/api", # API URI.
  :api_token => "Bearer xxxxxxxx", # JWT Token used for API Authentication.
  :api_verify_ssl => true, # true by default, optional.
  :verbose => false # false by default, optional. Prints logs.
)
```

### Synchronize a local model with API data

#### Class methods

A local model needs to implement 3 methods : `sync_create`, `sync_update`, `sync_delete_unsynchronized`.

```ruby
class Country < ActiveRecord::Base
  # create a new entry
  def self.sync_create(data)
    # self is the model class.
    self.create({
      :uid => data["id"].to_s,
      :labelFr => data["labelFr"].to_s,
      :labelFr => data["labelEn"].to_s,
      # ...
    })
  end
  
  # update an existing entry
  def sync_update(data)
    # self is the object that needs to be updated.
    self.update(
      :labelFr => data["labelFr"].to_s,
      :labelFr => data["labelEn"].to_s,
      # ...
    )
  end
  
  # delete all unsynchronized entries
  def self.sync_delete_unsynchronized(synchronized_keys)
    synchronized_ids = self.where(:key_field => synchronized_keys).pluck(:id).uniq
    entries_to_delete = self.where.not(:id => synchronized_ids).all
  
    # Apply specific rules when doing object deletion
    entries_to_delete.each do |entry|
      # ... Do something
    end
    
    # Or delete everything
    entries_to_delete.destroy_all
  end
  
  # Other class methods...
end
```

#### ApiSync::Synchronizer

ApiSync::Synchronizer exploits the three methods defined in the model, and synchronize it with a distant repository provided by the API.

```ruby
local_model = {
  :name => "Countries", # optional, for logs
  :class => Country, # local model class 
  :mapping_attribute => :uid # mapping attribute in the local model.
}

distant_model = {
  :endpoint => ApiSync::Language::ENDPOINT_COUNTRIES, # api endpoint.
  :mapping_attribute => "id" # mapping attribute in api data received.
}

ApiSync::Synchronizer.sync(local_model, distant_model)
```

Additionally, we can also synchronize with parameters if we want to synchronize a particular set of the api data and not everything.

As an example, we want to synchronize only english-speaking countries. The API has a filter `officialLanguage` :

```ruby
local_model = {
  :name => "Countries", # optional, for logs
  :class => Country, # local model class 
  :mapping_attribute => :uid # mapping attribute in the local model.
}

distant_model = {
  :endpoint => ApiSync::Language::ENDPOINT_COUNTRIES, # api endpoint.
  :mapping_attribute => "id" # mapping attribute in api data received.
}

parameters = {
  "officialLanguage" => "english"
}

ApiSync::Synchronizer.sync_with_parameters(local_model, distant_model, parameters)
```

### ApiSync::Client

We can execute requests directly without using the synchronizer.

```ruby
# Get all results
response = ApiSync::Client::get(:resource => ApiSync::Language::ENDPOINT_COUNTRIES,)

# Get one result
response = ApiSync::Client::get(:resource => ApiSync::Language::ENDPOINT_COUNTRIES, :resource_id => '43')

# Get filtered results with parameters
parameters = {
  "officialLanguage" => "english"
}
response = ApiSync::Client::get(:resource => ApiSync::Language::ENDPOINT_COUNTRIES, :parameters => parameters)
```

Note that `response` will need to be parsed into a hash.
```ruby
require 'json'
# ...
hash = JSON.parse(response.body)
datas = hash["hydra:member"]
```

## Development

// TODO
