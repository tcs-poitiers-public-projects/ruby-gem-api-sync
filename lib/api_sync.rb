# api sync files exposed to the app
require 'api_sync/client'
require 'api_sync/log_printer'
require 'api_sync/models'
require 'api_sync/synchronizer'
require 'api_sync/helpers/language'
require 'api_sync/helpers/response_parser'

module ApiSync
  def self.init(api_url:, api_token:, api_verify_ssl: true, verbose: false)
    ApiSync::Client::configure(api_url, api_token, api_verify_ssl)
    ApiSync::LogPrinter::set_verbose(verbose)
  end
end