require 'api_sync/models'

module ApiSync
  module Validators
    module ModelValidator

      ALLOWED_MODELS = [
        ApiSync::Models::Civilities,
        ApiSync::Models::Companies,
        ApiSync::Models::Countries,
        ApiSync::Models::Diplomas,
        ApiSync::Models::FinancingModes,
        ApiSync::Models::FrenchCities,
        ApiSync::Models::MaritalStatuses,
        ApiSync::Models::OriginDiplomas,
        ApiSync::Models::Programs,
        ApiSync::Models::RecruitmentLevels,
        ApiSync::Models::Schools,
        ApiSync::Models::Structures,
        ApiSync::Models::Students,
        ApiSync::Models::Users,
      ]

      def self.validate_api_model(api_model)
        if self::ALLOWED_MODELS.include?(api_model)
          return
        end

        msg = "api_model must be one of the following values : "
        self::ALLOWED_MODELS.each do |model|
          msg = msg + model.name + ", "
        end
        raise Exception.new msg
      end

      def self.validate_api_sync_attribute(api_model, api_sync_attribute)
        if api_sync_attribute.nil? || api_model.is_allowed_sync_attribute?(api_sync_attribute)
          return
        end

        msg = "api_sync_attribute must be one of the following values : "
        api_model.get_allowed_sync_attributes.each do |attribute|
          msg = msg + attribute + ", "
        end
        raise Exception.new msg
      end
    end
  end
end