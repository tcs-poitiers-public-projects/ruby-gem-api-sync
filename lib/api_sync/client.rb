require 'rest-client'
require 'api_sync/validators/model_validator'

module ApiSync
  class Client

    private

    # Data format sent/accepted
    JSON_LD = "application/ld+json"
    FORM_DATA = "multipart/form-data"

    public

    # configuration setter.
    def self.configure(api_url, api_token, api_verify_ssl = false)
      @api_uri = api_url
      @api_token = api_token
      @api_verify_ssl = api_verify_ssl
    end

    # GET / POST / PUT / DELETE requests.

    # Get requests
    def self.get(api_model:, resource_id: '', parameters: {})
      self::validate_api_params
      ApiSync::Validators::ModelValidator::validate_api_model(api_model)

      url = self::build_url(api_model.get_endpoint, resource_id, parameters)

      response = self::do_request(
        :url => url,
        :method => :get,
        :headers => {
          Authorization: @api_token,
          accept: self::JSON_LD
        },
        :verify_ssl => @api_verify_ssl
      )

      # 200 => resource given
      # 404 => resource not found (in case of resource_id given).
      return response
    end

    # POST ressource with JSON data
    def self.post(api_model:, json_datas:)
      self::validate_api_params
      ApiSync::Validators::ModelValidator::validate_api_model(api_model)

      url = self::build_url(api_model.get_endpoint)

      response = self::do_request(
        :url => url,
        :method => :post,
        :payload => json_datas,
        :headers => {
          Authorization: @api_token,
          content_type: self::JSON_LD,
          accept: self::JSON_LD
        },
        :verify_ssl => @api_verify_ssl
      )

      # 201 => resource created
      # 400 => invalid input
      # 422 => UnprocessableEntity
      return response
    end

    def self.post_student_photo(student_id:, photo_file:)
      self::validate_api_params

      url = @api_uri+'/'+ApiSync::Models::Students.get_photo_endpoint(student_id)

      response = self::do_request(
        :url => url,
        :method => :post,
        :payload => {
          :photo => photo_file
        },
        :headers => {
          Authorization: @api_token,
          content_type: self::FORM_DATA,
          accept: self::JSON_LD
        },
        :verify_ssl => @api_verify_ssl
      )

      # 201 => resource created
      # 400 => invalid input
      # 404 => resource not found
      return response
    end

    def self.get_student_photo(student_id:)
      self::validate_api_params

      url = @api_uri+'/'+ApiSync::Models::Students.get_photo_endpoint(student_id)

      response = self::do_request(
        :url => url,
        :method => :get,
        :headers => {
          Authorization: @api_token,
          accept: self::JSON_LD
        },
        :verify_ssl => @api_verify_ssl
      )

      # 200 => photo given.
      # 404 => student not found.
      # 204 => student found but has no photo.
      return response
    end

    private

    # Validators before calls
    def self.validate_api_params
      if @api_uri.blank? || @api_token.blank? || @api_verify_ssl.nil?
        raise Exception.new "Config for api calls is not defined."
      end
    end

    # Helpers
    def self.build_url(resource, resource_id = nil, parameters = {})
      url = @api_uri+'/'+resource
      if !resource_id.blank?
        url = url + '/'+resource_id.to_s
      end
      if !parameters.blank?
        url = url + self::format_parameters_url_string(parameters)
      end
      return url
    end

    def self.format_parameters_url_string(parameters)
      parameters_array = []
      parameters.each do |key, value|
        parameters_array << key.to_s+'='+value.to_s
      end
      return '?'+parameters_array.join('&')
    end

    def self.do_request(url:, method:, headers:, payload: nil, verify_ssl:)
      begin
        return RestClient::Request.execute(
          :url => url,
          :method => method,
          :headers => headers,
          :payload => payload,
          :verify_ssl => verify_ssl
        )
      rescue RestClient::Exception => e
        return e.response
      end
    end
  end
end