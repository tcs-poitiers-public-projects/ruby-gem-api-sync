require 'api_sync/helpers/response_parser'

module ApiSync
  class Synchronizer

    # Synchronizes all entries of a local model with data fetched from a distant api repository.
    #
    # Arguments :
    # - name : model name, for logging purposes.
    # - model_class : class name of the local model. The class must :
    #   * implement the constant SYNC_ATTRIBUTE : the local mapping key attribute used to map existing data with api data.
    #   * implement self.sync_create(data), sync_update(data), self.sync_delete_unsynchronized(synchronized_keys)
    # - api_model : a class from ApiSync::Models representing the distant api_repository. (ex : ApiSync::Models::Countries)
    # - api_sync_attribute : optionnal. Overrides the default api mapping key attribute. Default is ID. (ApiSync::Models::Countries::ID).
    # - parameters : optionnal. Parameters for get request, added to the URL. Unknown parameters will be ignored.
    #
    # Usage example :
    #     ApiSync::Synchronizer.sync(
    #       :name => "Countries",
    #       :model_class => CountryItem,
    #       :api_model => ApiSync::Models::Countries,
    #       :api_sync_attribute => ApiSync::Models::Countries::UID,
    #     )
    def self.sync(name:, model_class:, api_model:, api_sync_attribute: nil, parameters: {})
      self::validate_model_class(model_class)

      ApiSync::LogPrinter::render_header(name)
      response = ApiSync::Client::get(:api_model => api_model, :parameters => parameters)

      if response.code != 200
        ApiSync::LogPrinter::render_failure("Response received is not OK : code "+response.code.to_s+" - "+response.message)
        ApiSync::LogPrinter::render_footer("failure")
        return
      end

      ApiSync::LogPrinter::render_success
      new_datas = ApiSync::Helpers::ResponseParser::parse(response)

      ApiSync::Validators::ModelValidator::validate_api_sync_attribute(api_model, api_sync_attribute)
      if api_sync_attribute.nil?
        api_sync_attribute = api_model.get_default_sync_attribute
      end

      synchronized_keys = self::sync_referential(
        :model_class => model_class,
        :api_sync_attribute => api_sync_attribute,
        :new_datas => new_datas,
      )
      ApiSync::LogPrinter::render_result(synchronized_keys.length)

      # Delete unsynchronized entries
      model_class.sync_delete_unsynchronized(synchronized_keys)

      ApiSync::LogPrinter::render_footer("success")
    end

    private

    # Synchronize data : create, or update.
    # Returns keys of synchronized entries (the value of the mapping attribute).
    def self.sync_referential(model_class:, api_sync_attribute:, new_datas:, synchronized_keys: [])

      model_sync_attribute = model_class.const_get(:SYNC_ATTRIBUTE)

      new_datas.each do |new_data|
        key_value = new_data[api_sync_attribute]

        local_entry = model_class.find_by(model_sync_attribute => key_value)
        if local_entry
          local_entry.sync_update(new_data) # update values and save.
        else
          model_class.sync_create(new_data) # create entry and save.
        end

        synchronized_keys << key_value
      end

      return synchronized_keys
    end

    def self.validate_model_class(model_class)

      if !model_class.const_defined?(:SYNC_ATTRIBUTE)
        raise Exception.new "Class "+model_class.name+" must implement the constant SYNC_ATTRIBUTE."
      end

      class_methods = model_class.methods(false) # static methods list
      instance_methods = model_class.instance_methods(false) # instance methods list

      if !class_methods.include?(:sync_create)
        raise Exception.new "Class "+model_class.name+" must implement the method self.create(data)."
      end
      if !instance_methods.include?(:sync_update)
        raise Exception.new "Class "+model_class.name+" must implement the method update(data)."
      end
      if !class_methods.include?(:sync_delete_unsynchronized)
        raise Exception.new "Class "+model_class.name+" must implement the method self.sync_delete_unsynchronized(synchronized_keys)."
      end
    end
  end
end