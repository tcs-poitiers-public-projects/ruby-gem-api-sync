require 'api_sync/models/base_model'

module ApiSync
  module Models
    class Countries < BaseModel

      UID = "labelId"
      LABEL_FR = "labelFr"
      LABEL_EN = "labelEn"
      LABEL_F_NAT_FR = "femaleNationalityFr"
      LABEL_F_NAT_EN = "femaleNationalityEn"
      LABEL_M_NAT_FR = "maleNationalityFr"
      LABEL_M_NAT_EN = "maleNationalityEn"

      # Specific attributes values in Agate used in apps.
      FRANCE_ID = 100

      def self.get_endpoint
        return "countries"
      end

      def self.get_allowed_sync_attributes
        return ApiSync::Models::BaseModel.get_allowed_sync_attributes + [ self::UID ]
      end
    end
  end
end
