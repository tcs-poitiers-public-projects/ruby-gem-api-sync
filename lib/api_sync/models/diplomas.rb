require 'api_sync/models/base_model'

module ApiSync
  module Models
    class Diplomas < BaseModel

      UID = "labelId"
      LABEL_FR = "labelFr"
      LABEL_EN = "labelEn"

      # Specific attributes values in Agate used in apps.
      DIS_ID = 10
      DESA_ID = 18

      def self.get_endpoint
        return "degrees"
      end

      def self.get_allowed_sync_attributes
        return ApiSync::Models::BaseModel.get_allowed_sync_attributes + [ self::UID ]
      end
    end
  end
end
