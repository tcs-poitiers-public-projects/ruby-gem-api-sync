require 'api_sync/models/base_model'

module ApiSync
  module Models
    class FinancingModes < BaseModel

      UID = "labelId"
      LABEL_FR = "labelFr"
      LABEL_EN = "labelEn"

      # Specific attributes values in Agate used in apps.
      DEFAULT_ID = 36 # "En attente"
      PST_ID = 26
      APPRENTICESHIP_UID = 29
      SPONSORING_ID = 21
      SANDWICH_COURSE_SPONSORING_UID = 30
      PROFESSIONAL_LEAVE_ID = 28
      MILITARY_SERVICE_LEAVE_ID = 27

      def self.get_endpoint
        return "financings"
      end

      def self.get_allowed_sync_attributes
        return ApiSync::Models::BaseModel.get_allowed_sync_attributes + [ self::UID ]
      end
    end
  end
end
