require 'api_sync/models/base_model'

module ApiSync
  module Models
    class Civilities < BaseModel

      UID = "labelId"
      LABEL_FR = "labelFr"
      LABEL_EN = "labelEn"

      # Specific attributes values in Agate used in apps.
      MAN_ID = "M."

      def self.get_endpoint
        return "genders"
      end

      def self.get_allowed_sync_attributes
        return ApiSync::Models::BaseModel.get_allowed_sync_attributes + [ self::UID ]
      end
    end
  end
end