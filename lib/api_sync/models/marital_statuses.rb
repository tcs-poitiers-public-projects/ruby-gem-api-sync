require 'api_sync/models/base_model'

module ApiSync
  module Models
    class MaritalStatuses < BaseModel

      UID = "labelId"
      LABEL_FR = "labelFr"
      LABEL_EN = "labelEn"

      def self.get_endpoint
        return "family_statuses"
      end

      def self.get_allowed_sync_attributes
        return ApiSync::Models::BaseModel.get_allowed_sync_attributes + [ self::UID ]
      end
    end
  end
end
