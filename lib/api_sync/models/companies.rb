require 'api_sync/models/base_model'

module ApiSync
  module Models
    class Companies < BaseModel

      UID = "identifier"
      NAME = "name"

      def self.get_endpoint
        return "companies"
      end

      def self.get_allowed_sync_attributes
        return ApiSync::Models::BaseModel.get_allowed_sync_attributes + [ self::UID ]
      end
    end
  end
end
