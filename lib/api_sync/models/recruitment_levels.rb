require 'api_sync/models/base_model'

module ApiSync
  module Models
    class RecruitmentLevels < BaseModel

      UID = "labelId"
      LABEL_FR = "labelFr"
      LABEL_EN = "labelEn"

      # Specific attributes values in Agate used in apps.
      BAC5_CTI_WITH_EXP_ID = 1
      BAC5_CTI_WITHOUT_EXP_ID = 2
      BAC5_OTHER_ID = 3
      BAC4_CONVENTION_ID = 4
      BAC4_OTHER_ID = 5
      UNIVERSITARY_ID = 6
      PST_ID = 7

      def self.get_endpoint
        return "recruiting_modes"
      end

      def self.get_allowed_sync_attributes
        return ApiSync::Models::BaseModel.get_allowed_sync_attributes + [ self::UID ]
      end
    end
  end
end
