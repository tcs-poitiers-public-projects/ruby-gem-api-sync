require 'api_sync/models/base_model'

module ApiSync
  module Models
    class FrenchCities < BaseModel

      NAME = "name"
      POSTAL_CODE = "zipCode"
      INSEE_CODE = "inseeCode"
      INSEE_ENRICHED_NAME = "inseeEnrichedName"
      LOCALITY = "locality"
      LA_POSTE_INSEE_CODE = "laPosteInseeCode"
      LA_POSTE_NAME = "laPosteName"


      def self.get_endpoint
        return "french_cities"
      end

    end
  end
end
