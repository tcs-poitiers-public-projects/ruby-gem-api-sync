require 'api_sync/models/base_model'

module ApiSync
  module Models
    class Programs < BaseModel

      UID = "labelId"
      LABEL_FR = "labelFr"
      LABEL_EN = "labelEn"
      STRUCTURE = "structure"
      LANGUAGE = "language"
      ARCHIVED_DATE = "archivedAt"

      FILTER_WITH_ARCHIVED = "withArchived"

      def self.get_endpoint
        return "programs"
      end

      def self.get_allowed_sync_attributes
        return ApiSync::Models::BaseModel.get_allowed_sync_attributes + [ self::UID ]
      end
    end
  end
end
