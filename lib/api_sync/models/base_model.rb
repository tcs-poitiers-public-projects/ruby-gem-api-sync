module ApiSync
  module Models
    class BaseModel

      ID = "id"

      def self.get_endpoint
        return self::ENDPOINT
      end

      def self.get_default_sync_attribute
        return self::ID
      end

      def self.get_allowed_sync_attributes
        return [ self::ID ]
      end

      def self.is_allowed_sync_attribute?(sync_attribute)
        return self.get_allowed_sync_attributes.include?(sync_attribute)
      end

    end
  end
end