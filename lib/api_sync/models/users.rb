require 'api_sync/models/base_model'

module ApiSync
  module Models
    class Users < BaseModel

      UID = "login"
      FAMILY_NAME = "familyName"
      FIRST_NAME = "firstName"
      EMAIL = "email"
      PREFERRED_LANGUAGE = "preferredLanguage"

      FILTER_HAS_ALTERNANTS_ACCESS = "hasAlternantApplicationAccess"
      FILTER_HAS_CANDIDATE_ACCESS = "hasCandidateApplicationAccess"
      FILTER_HAS_CANDIDATURE_ACCESS = "hasCandidatureApplicationAccess"

      def self.get_endpoint
        return "staff"
      end

      def self.get_default_sync_attribute
        return self::UID
      end

      def self.get_allowed_sync_attributes
        return ApiSync::Models::BaseModel.get_allowed_sync_attributes + [ self::UID ]
      end
    end
  end
end
