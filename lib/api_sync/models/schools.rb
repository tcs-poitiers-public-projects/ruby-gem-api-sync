require 'api_sync/models/base_model'

module ApiSync
  module Models
    class Schools < BaseModel

      ACRONYM = "acronym"
      NAME = "name"
      TYPE = "type"
      CITY = "city"
      COUNTRY = "country"
      IS_REGISTERED = "isRegistered"
      IS_PARTNER = "isPartner"

      def self.get_endpoint
        return "schools"
      end

      def self.get_allowed_sync_attributes
        return ApiSync::Models::BaseModel.get_allowed_sync_attributes + [ self::ACRONYM ]
      end
    end
  end
end
