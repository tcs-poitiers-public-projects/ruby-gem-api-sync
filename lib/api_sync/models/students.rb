require 'api_sync/models/base_model'

module ApiSync
  module Models
    class Students < BaseModel
      # Personal information about students
      UID = "login"
      FAMILY_NAME = "familyName"
      USUAL_NAME = "usualName"
      USUAL_FIRSTNAME = "usualFirstname"
      OTHER_FIRSTNAMES = "otherFirstnames"
      BIRTH_DATE = "birthDate"
      NAT_1 = "firstNationality"
      NAT_2 = "secondNationality"
      CITY = "city"
      COUNTRY = "country"
      PREFERRED_LANGUAGE = "preferredLanguage"
      PERSONAL_ADDRESS = "personalAddress"
      PERSONAL_PHONE = "personalPhone"
      PERSONAL_EMAIL = "personalEmail"
      URGENCY_ADDRESS = "urgencyAddress"
      URGENCY_PHONE = "urgencyPhone"
      # School related information about student
      INE = "ine"
      INSCRIPTION_NUMBER = "inscriptionNumber"
      RECRUITMENT_LEVEL = "recruitment"
      ORIGIN_SCHOOL = "originSchool"
      DIPLOMA = "wantedDegree"
      PROGRAM = "program"
      PROMOTION = "promotion"
      FINANCING_MODE = "financing"
      FINAL_SPONSOR = "finalSponsor"
      UNIVERSITY_SPONSOR = "university_sponsor"
      SCHOOLING_DURATION = "schoolingDuration"
      SCHOOLING_START_DATE = "schoolingStartAt"
      SCHOOLING_END_DATE = "schoolingEndAt"

      # Endpoint filters
      FILTER_LIFETIME_EMAIL = "lifetimeEmail"
      FILTER_HAS_ECAMPUS_ACCESS = "hasECampusAccess"
      FILTER_HAS_LMS_ACCESS = "hasLMSAccess"
      FILTER_GENERIC_STUDENT = "genericAccount"

      def self.get_endpoint
        return "students"
      end

      def self.get_photo_endpoint(resource_id)
        return "students/"+resource_id.to_s+"/photo"
      end

      def self.get_default_sync_attribute
        return self::UID
      end

      def self.get_allowed_sync_attributes
        return ApiSync::Models::BaseModel.get_allowed_sync_attributes + [ self::UID ]
      end
    end
  end
end
