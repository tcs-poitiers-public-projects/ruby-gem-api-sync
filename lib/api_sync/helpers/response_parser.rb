require 'json'
require 'stringio'

module ApiSync
  module Helpers
    module ResponseParser

      # MIME types that we can receive in a response.
      JSON_LD = "application/ld+json; charset=utf-8"

      IMG_GIF = "image/gif"
      IMG_JPEG = "image/jpeg"
      IMG_PNG = "image/png"

      IMG_MIME_TYPES = [ self::IMG_GIF, self::IMG_JPEG, self::IMG_PNG ]

      def self.parse(response)
        content_type = response.headers[:content_type]

        if content_type == self::JSON_LD
          return self::json_parse(response)
        end

        if self::IMG_MIME_TYPES.include?(content_type)
          return self::image_parse(response)
        end

        raise Exception.new "The type " + content_type + " is currently not parsed by the ResponseParser module."
      end

      private

      # Returns hash of multiple entries, or a single entry.
      def self.json_parse(response)
        res = JSON.parse(response.body)

        # Multiple results
        if res.key?("hydra:member")
          datas = res["hydra:member"]
          return datas
        end

        # Single result
        datas = []
        datas << res

        return datas
      end

      # Returns hash with image metadata and data.
      def self.image_parse(response)
        binary_content = response.body
        content_type = response.headers[:content_type]
        filename_ext = content_type.split('/')[1]

        return {
          :content_type => content_type,
          :filename_ext => filename_ext,
          :io => StringIO.new(binary_content)
        }
      end

    end
  end
end