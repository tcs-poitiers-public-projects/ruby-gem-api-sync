module ApiSync
  module Helpers
    module Language

      # Specific attributes values in Agate.
      FR = 10
      EN = 20

      def self.get_locale_from_id(language_id)
        case language_id
        when self::FR
          return "fr"
        when self::EN
          return "en"
        else
          return nil
        end
      end

    end
  end
end