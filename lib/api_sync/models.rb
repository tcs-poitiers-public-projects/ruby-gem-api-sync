require 'api_sync/models/civilities'
require 'api_sync/models/companies'
require 'api_sync/models/countries'
require 'api_sync/models/diplomas'
require 'api_sync/models/financing_modes'
require 'api_sync/models/french_cities'
require 'api_sync/models/marital_statuses'
require 'api_sync/models/origin_diplomas'
require 'api_sync/models/programs'
require 'api_sync/models/recruitment_levels'
require 'api_sync/models/schools'
require 'api_sync/models/structures'
require 'api_sync/models/students'
require 'api_sync/models/users'

module ApiSync
  module Models

  end
end