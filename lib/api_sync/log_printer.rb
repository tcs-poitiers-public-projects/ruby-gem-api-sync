module ApiSync
  module LogPrinter

    def self.set_verbose(verbose = false)
      @verbose = verbose
    end

    def self::render_message(message)
      if @verbose
        puts message
      end
    end

    def self.render_header(repository_name)
      self::render_message(
        [
          "=====================================",
          "::::::::::::: "+repository_name+" :::::::::::::",
          "Request to API..."
        ])
    end

    def self.render_success
      self::render_message("Data received. Update...")
    end

    def self.render_paginated_success(page, total)
      msg = "Data received " + page.to_s + "/" + total.to_s + ". Update..."
      self::render_message(msg)
    end

    def self.render_failure(failure_details)
      self::render_message(
        [
          "Failure to receive data. Details :",
          "-------------------------------------",
          failure_details
        ])
    end

    def self.render_result(number)
      msg = number.to_s + " entries have been synchronized."
      self::render_message(msg)
    end

    def self.render_footer(status)
      self::render_message(
        [
          "Synchronization status : "+status+".",
          "====================================="
        ])
    end

    def self.render_delete_report(details: [], to_delete_count: -1, deleted_count: -1, undeleted_count: -1)
      msg = [
        "========================",
        "Delete unsynchronized entries",
        "========================"
      ]

      msg = msg + details

      msg << "========================"

      if (to_delete_count < 0) && (deleted_count < 0) && (undeleted_count < 0)
        self::render_message(msg)
        return
      end

      if to_delete_count >= 0
        msg << to_delete_count.to_s + " entries were marked for deletion."
      end

      if deleted_count >= 0
        msg << deleted_count.to_s + " entries were deleted."
      end

      if undeleted_count >= 0
        msg << undeleted_count.to_s + " entries could not be deleted."
      end
      msg << "========================"

      self::render_message(msg)
    end

  end
end